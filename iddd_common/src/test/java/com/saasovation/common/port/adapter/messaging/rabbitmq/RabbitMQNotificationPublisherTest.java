package com.saasovation.common.port.adapter.messaging.rabbitmq;

import java.util.Date;

import com.saasovation.common.CommonTestCase;
import com.saasovation.common.domain.model.DomainEvent;
import com.saasovation.common.domain.model.DomainEventPublisher;
import com.saasovation.common.event.EventStore;
import com.saasovation.common.event.TestableDomainEvent;
import com.saasovation.common.notification.NotificationPublisher;
import com.saasovation.common.notification.PublishedNotificationTrackerStore;
import com.saasovation.common.persistence.PersistenceManagerProvider;
import com.saasovation.common.port.adapter.notification.RabbitMQNotificationPublisher;
import com.saasovation.common.port.adapter.persistence.hibernate.HibernateEventStore;
import com.saasovation.common.port.adapter.persistence.hibernate.HibernatePublishedNotificationTrackerStore;

public class RabbitMQNotificationPublisherTest extends CommonTestCase {

	public RabbitMQNotificationPublisherTest() {
		super();
	}

	public void testPublishNotifications() throws Exception {
		EventStore eventStore = this.eventStore();

		assertNotNull(eventStore);

		PublishedNotificationTrackerStore publishedNotificationTrackerStore = new HibernatePublishedNotificationTrackerStore(new PersistenceManagerProvider(this.session()), "unit.test");

		NotificationPublisher notificationPublisher = new RabbitMQNotificationPublisher(eventStore, publishedNotificationTrackerStore, "unit.test");

		assertNotNull(notificationPublisher);

		notificationPublisher.publishNotifications();
	}

	@Override
	protected void setUp() throws Exception {
		DomainEventPublisher.instance().reset();

		super.setUp();

		// always start with at least 20 events

		EventStore eventStore = this.eventStore();

		long startingDomainEventId = (new Date()).getTime();

		for (int idx = 0; idx < 20; ++idx) {
			long domainEventId = startingDomainEventId + 1;

			DomainEvent event = new TestableDomainEvent(domainEventId, "name" + domainEventId);

			eventStore.append(event);
		}
	}

	private EventStore eventStore() {
		EventStore eventStore = new HibernateEventStore(new PersistenceManagerProvider(this.session()));

		assertNotNull(eventStore);

		return eventStore;
	}
}
