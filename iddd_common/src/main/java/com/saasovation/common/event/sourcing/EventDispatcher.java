package com.saasovation.common.event.sourcing;

public interface EventDispatcher {

	void dispatch(DispatchableDomainEvent aDispatchableDomainEvent);

	void registerEventDispatcher(EventDispatcher anEventDispatcher);

	boolean understands(DispatchableDomainEvent aDispatchableDomainEvent);
}
