package com.saasovation.common.event.sourcing;

public interface EventNotifiable {

	void notifyDispatchableEvents();
}
