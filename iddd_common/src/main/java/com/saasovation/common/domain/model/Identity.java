package com.saasovation.common.domain.model;

/**
 * 身份标识
 * 
 * @author Darkness
 * @date 2014-5-28 下午9:30:00
 * @version V1.0
 */
public interface Identity {

	String id();
}
