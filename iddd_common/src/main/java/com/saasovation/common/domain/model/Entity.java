package com.saasovation.common.domain.model;

/**
 * 实体基类
 * 
 * @author Darkness
 * @date 2014-5-28 下午9:25:33
 * @version V1.0
 */
public class Entity extends IdentifiedDomainObject {

    private static final long serialVersionUID = 1L;

    protected Entity() {
        super();
    }
}
