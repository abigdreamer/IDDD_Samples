package com.saasovation.common.domain.model;

public interface DomainEventSubscriber<T> {

	void handleEvent(final T aDomainEvent);

	Class<T> subscribedToEventType();
}
