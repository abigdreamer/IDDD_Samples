package com.saasovation.common.persistence;

/**
 * 可清除的存储
 * 
 * @author Darkness
 * @date 2014-5-28 下午8:07:28
 * @version V1.0
 */
public interface CleanableStore {

	void clean();
}
