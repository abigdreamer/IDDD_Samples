package com.saasovation.identityaccess.infrastructure.persistence;

import java.util.Collection;

import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.exception.ConstraintViolationException;

import com.saasovation.common.port.adapter.persistence.hibernate.AbstractHibernateSession;
import com.saasovation.identityaccess.domain.model.access.Role;
import com.saasovation.identityaccess.domain.model.identity.TenantId;
import com.saasovation.identityaccess.domain.repository.RoleRepository;

/**
 * 角色仓储Hibernate实现
 * 
 * @author Darkness
 * @date 2014-5-28 下午10:23:07
 * @version V1.0
 */
public class HibernateRoleRepository
        extends AbstractHibernateSession
        implements RoleRepository {

    public HibernateRoleRepository() {
        super();
    }

    @Override
    public void add(Role aRole) {
        try {
            this.session().saveOrUpdate(aRole);
        } catch (ConstraintViolationException e) {
            throw new IllegalStateException("Role is not unique.", e);
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public Collection<Role> allRoles(TenantId aTenantId) {
        Query query = this.session().createQuery(
                "from com.saasovation.identityaccess.domain.model.access.Role as _obj_ "
                + "where _obj_.tenantId = ?");

        query.setParameter(0, aTenantId);

        return (Collection<Role>) query.list();
    }

    @Override
    public void remove(Role aRole) {
        this.session().delete(aRole);
    }

    @Override
    public Role roleNamed(TenantId aTenantId, String aRoleName) {
        Query query = this.session().createQuery(
                "from com.saasovation.identityaccess.domain.model.access.Role as _obj_ "
                + "where _obj_.tenantId = ? "
                  + "and _obj_.name = ?");

        query.setParameter(0, aTenantId);
        query.setParameter(1, aRoleName, Hibernate.STRING);

        return (Role) query.uniqueResult();
    }
}
