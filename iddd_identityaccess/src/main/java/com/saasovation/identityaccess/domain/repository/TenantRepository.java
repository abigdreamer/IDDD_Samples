package com.saasovation.identityaccess.domain.repository;

import com.saasovation.identityaccess.domain.model.identity.Tenant;
import com.saasovation.identityaccess.domain.model.identity.TenantId;

/**
 * 租赁仓储
 * 
 * @author Darkness
 * @date 2014-5-27 下午7:55:46
 * @version V1.0
 */
public interface TenantRepository {

	void add(Tenant aTenant);

	void remove(Tenant aTenant);
	
	TenantId nextIdentity();

	Tenant tenantOfId(TenantId aTenantId);
	
	Tenant tenantNamed(String aName);
}
