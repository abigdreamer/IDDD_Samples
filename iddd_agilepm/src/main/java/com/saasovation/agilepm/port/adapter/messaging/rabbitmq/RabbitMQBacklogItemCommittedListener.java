package com.saasovation.agilepm.port.adapter.messaging.rabbitmq;

import com.saasovation.agilepm.application.sprint.*;
import com.saasovation.common.notification.NotificationReader;
import com.saasovation.common.port.adapter.messaging.Exchanges;
import com.saasovation.common.port.adapter.messaging.rabbitmq.ExchangeListener;

public class RabbitMQBacklogItemCommittedListener extends ExchangeListener {

    private SprintApplicationService sprintApplicationService;

    public RabbitMQBacklogItemCommittedListener(
            SprintApplicationService aSprintApplicationService) {

        super();

        this.sprintApplicationService = aSprintApplicationService;
    }

    @Override
    protected String exchangeName() {
        return Exchanges.AGILEPM_EXCHANGE_NAME;
    }

    @Override
    protected void filteredDispatch(String aType, String aTextMessage) {
        NotificationReader reader = new NotificationReader(aTextMessage);

        String tenantId = reader.eventStringValue("tenant.id");
        String backlogItemId = reader.eventStringValue("backlogItemId.id");
        String committedToSprintId = reader.eventStringValue("committedToSprintId.id");

        this.sprintApplicationService().commitBacklogItemToSprint(
                new CommitBacklogItemToSprintCommand(
                    tenantId,
                    committedToSprintId,
                    backlogItemId));
    }

    @Override
    protected String[] listensTo() {
        return new String[] {
                "com.saasovation.agilepm.domain.model.product.backlogitem.BacklogItemCommitted"
        };
    }

    private SprintApplicationService sprintApplicationService() {
        return this.sprintApplicationService;
    }
}
